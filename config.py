port=8889
# It's OK to put ~ in the dir name
user_file_dir="~/tmp/speechfiles"
# Note that the file locations that are given to speech2text must be relative to the Docker container
# The {audio} and {json} placeholders are replaced
transcribe_command="docker exec -t speech2text /opt/kaldi-offline-transcriber/speech2text.sh --json /opt/speechfiles/{json} /opt/speechfiles/{audio}"

num_transcription_processes=4