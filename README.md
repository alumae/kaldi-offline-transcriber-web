## Dependencies

  * Python 3.5 or later

## Installation

Clone this git repository:

    git clone <repo>
    
Change to the directory:

    cd kaldi-offline-transcriber-web
    
Install required Python packages. The pip command should correspond to Python 3 pip.

    pip install --no-cache-dir -r requirements.txt
    
Open `config.py` and change the port to appropritae value. Also the `user_file_dir` 
property should point to the same directory that is mapped to Docker container speech2text's
/opt/speechfiles directory. You don't have to change the `transcribe_command` property
if you run speech2text 'normally'.

## Running 

To run the server (`python` should point to Python 3.5 or later):

    python main.py --config=config.py
    
### Defining the number of concurrent transcription processes

The `num_transcription_processes` configuration variable defines the number of concurrent
transcription processes. If a transcription request is made when the number of concurrent
transcription processes exceeds this number, the request is put to a queue.

Note that the queue is not persistent across server restarts.

## Transcribing a file

(In another terminal), make a PUT request to the server, e.g.:

    curl -s -v -T sample/intervjuu2018101605_trimmed.mp3 'http://localhost:8889/upload?callback=http://localhost:8890/notify&extension=mp3'
        
If the audio file exists, the server will respond with HTTP status 200 (i.e., successful):

    HTTP/1.1 200 OK
    Server: TornadoServer/4.5.3
    Content-Type: text/html; charset=UTF-8
    Date: Tue, 16 Oct 2018 10:35:16 GMT
    X-Request-Id: ea76c985-51d2-41be-b9b7-4e4d739c01ae
    Content-Length: 0

Note that one of the response headers is `X-Request-Id`, which specifies the
request ID for the upload. This could be useful later.

The server should print something like that to output:

    [I 181016 13:35:16 web:2064] 200 PUT /upload?callback=http://localhost:8890/notify&extension=mp3 (::1) 27.96ms
    [I 181016 13:35:16 main:40] Starting to transcribe file
    [I 181016 13:35:16 main:43] Running command: docker exec -it speech2text /opt/kaldi-offline-transcriber/speech2text.sh --json /opt/speechfiles/ea76c985-51d2-41be-b9b7-4e4d739c01ae.json /opt/speechfiles/ea76c985-51d2-41be-b9b7-4e4d739c01ae.mp3
    
When the transcription process finishes, the server will make a callback PUT request to the URL specified in the upload request (http://localhost:8890/notify),
with an id=<X-request-Id> URL parameter, e.g. `http://localhost:8890/notify?id=ea76c985-51d2-41be-b9b7-4e4d739c01ae`,
with the following JSON structure:

    PUT /notify?id=91c65003-0d1b-4484-9fd5-3435ea9e2d44 HTTP/1.1
    Content-Type: application/json
    Connection: close
    Host: localhost:8890
    Content-Length: 25338
    Accept-Encoding: gzip

    {
        "id": "91c65003-0d1b-4484-9fd5-3435ea9e2d44",
        "metadata": {
            "version": "0.2"
        },
        "done": true,
        "result": {
            "speakers": {
                "S2": {
                    "name" : "Andres Kask"
                },
                "S0": {}
            },
            "sections": [
                {
                    "start": 0,
                    "type": "speech",
                    "end": 30.28,
                    "turns": [
                        {
                            "start": 0.0,
                            "speaker": "S0",
                            "end": 10.74,
                            "transcript": "Tartu ...",
                            "words": [
                                {
                                    "start": 0.0,
                                    "end": 0.42,
                                    "word": "Tartu",
                                    "punctuation": "",
                                    "word_with_punctuation": "Tartu"
                                },
                                ...


If the trancription process failed, the callback PUT request will contain the processing log in the JSON response:

    PUT /notify?id=f2fa7aac-6764-4ae7-b0bc-ec9df976263f HTTP/1.1
    Content-Type: application/json
    Connection: close
    Host: localhost:8890
    Content-Length: 3196
    Accept-Encoding: gzip

    {
        "id": "f2fa7aac-6764-4ae7-b0bc-ec9df976263f",
        "metadata": {
            "version": "0.2RC0.post5"
        },
        "done": true,
        "error": {
            "code": 1,
            "message": "Transcribing failed",
            "log": "/opt/kaldi-offline-transcriber/speech2text.sh --json /opt/speechfiles/f2fa7aac-6764-4ae7-b0bc-ec9df976263f.json /opt/speechfiles/f2fa7aac-6764-4ae7-b0bc-ec9df976263f.mp3\r\nmkdir -p `dirname build/audio/base/f2fa7aac-6764-4ae7-b0bc-ec9df976263f.wav`\r\nffmpeg ..."
        }
    }



You can also retrieve the transcription of the uploaded file any time, by making a GET request to `/result?id=<X-request-Id>`, e.g.
to `http://localhost:8889/result?id=ea76c985-51d2-41be-b9b7-4e4d739c01ae`. The response is always JSON-formatted, describing
either the transcription result, or giving reasons why the result could not be retrieved.

If the trancription request is still in the queue (or is in progress), the response will be something like that:

    HTTP/1.1 200 OK
    Server: TornadoServer/4.5.3
    Content-Type: application/json
    Date: Thu, 01 Nov 2018 14:29:43 GMT
    Etag: "19d769cc20ad9ad0526ad3f6ce41950510833157"
    Content-Length: 160

    {
        "id": "91c65003-0d1b-4484-9fd5-3435ea9e2d44",
        "metadata": {
            "version": "0.2"
        },
        "done": false,
        "message": "In progress"
    }

If the transcription has finished (and was successful), the response will look exactly like the JSON submitted to the callback URL:

    HTTP/1.1 200 OK
    Server: TornadoServer/4.5.3
    Content-Type: application/json
    Date: Thu, 01 Nov 2018 14:44:42 GMT
    Etag: "2acd5d3a8e609a86a357f87c94303735ede2368a"
    Content-Length: 25338
    
    {
        "id": "91c65003-0d1b-4484-9fd5-3435ea9e2d44",
        "metadata": {
            "version": "0.2"
        },
        "done": true,
        "result": {
            "speakers": {
                "S2": {
                    "name" : "Andres Kask"
                },
                "S0": {}
            },
            "sections": [
                {
                    "start": 0,
                    "type": "speech",
                    "end": 30.28,
                    "turns": [
                        {
                            "start": 0.0,
                            "speaker": "S0",
                            "end": 10.74,
                            "transcript": "Tartu ...",
                            "words": [
                                {
                                    "start": 0.0,
                                    "end": 0.42,
                                    "word": "Tartu",
                                    "punctuation": "",
                                    "word_with_punctuation": "Tartu"
                                },
                                ...



If the transcription process failed (or the ID is invalid), the response will look like this:

    HTTP/1.1 200 OK
    Server: TornadoServer/4.5.3
    Content-Type: application/json
    Date: Thu, 01 Nov 2018 14:47:07 GMT
    Etag: "a0c427562e9a4b145f7bc9364ddafe505e13ea5f"
    Content-Length: 226
    
    {
        "id": "91c65003-0d1b-4484-9fd5-3435ea9e2d44s",
        "metadata": {
            "version": "0.2"
        },
        "done": true,
        "error": {
            "code": 1,
            "message": "Result file for this ID not found"
        }
    }






