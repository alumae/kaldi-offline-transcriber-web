#!/usr/bin/env python3

import logging
import os
import shlex
import subprocess
import uuid
import enum
import json

import tornado.gen
import tornado.httpclient
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.process
import tornado.queues
import tornado.web
import tornado.log

import version

class RequestStates(enum.Enum):
    IN_QUEUE = 1
    PROCESSING = 2

class Application(tornado.web.Application):
    def __init__(self):
        self.ROOT = os.path.dirname(os.path.abspath(__file__))
        path = lambda root,*a: os.path.join(root, *a)
        settings = dict(
            cookie_secret="43oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            static_path=path(self.ROOT, 'static'),
            xsrf_cookies=False,
            autoescape=None,
            serve_traceback=True
        )

        handlers = [
            (r"/upload", UploadHandler),
            (r"/result", ResultHandler)
        ]

        tornado.web.Application.__init__(self, handlers, **settings)
        self.running_requests = dict()
        self.transciption_process_queue = tornado.queues.Queue()


async def worker(application):
    async for item in application.transciption_process_queue:
        try:
            request_id, audio_filename, json_filename, callback_url = item
            logging.info('Starting to transcribe request %s' % request_id)
            await start_transcribe(application, request_id, audio_filename, json_filename, callback_url)
        finally:
            application.transciption_process_queue.task_done()


async def start_transcribe(application, request_id, audio_filename, json_filename, callback_url):
    application.running_requests[request_id] = RequestStates.PROCESSING
    try:
        logging.info("Starting to transcribe file")        
        cmd = tornado.options.options.transcribe_command.format(audio=os.path.basename(audio_filename), json=os.path.basename(json_filename))
        tornado.process.Subprocess.initialize()
        logging.info("Running command: %s" % cmd)
        proc = tornado.process.Subprocess(shlex.split(cmd), stdout=tornado.process.Subprocess.STREAM, stderr=subprocess.STDOUT, cwd=application.ROOT, shell=False)
        output = (await proc.stdout.read_until_close()).decode("utf-8") 
        ret = await proc.wait_for_exit(raise_error=False)
        logging.info("Transcribing of request %s ended with status %d" % (request_id, ret))
        if callback_url:
            callback_url_with_id = callback_url + "?id=" + request_id
            logging.info("Making a callback request to " + callback_url_with_id)
            http_client = tornado.httpclient.AsyncHTTPClient()
            response = construct_response_json(request_id)
            response["done"] = True
            if ret == 0 and os.path.exists(json_filename):        
                result = json.load(open(json_filename))
                response["result"] = result
            else:
                response["error"] = {"code": 1, "message": "Transcribing failed", "log" : output}

            json_response = json.dumps(response, sort_keys=False, indent=4)
            http_client.fetch(callback_url_with_id, method="PUT", body=json_response, headers={"Content-Type": "application/json"})                
    finally:
        del application.running_requests[request_id]


def construct_response_json(request_id):
    response = {"id": request_id}
    response["metadata"] = {"version" : version.get_version(pep440=True)}
    return response


class UploadHandler(tornado.web.RequestHandler):

    def initialize(self):
        self.request_id = str(uuid.uuid4())
        
    def put(self, *args, **kwargs):
        callback_url = self.get_argument("callback", None)
        file_extension = self.get_argument("extension")
        file_body = self.request.body
        audio_filename = tornado.options.options.user_file_dir + "/" + self.request_id + "." + file_extension
        json_filename = tornado.options.options.user_file_dir + "/" + self.request_id + ".json" 
        with open(audio_filename, "wb") as f:
            f.write(file_body)
        self.application.transciption_process_queue.put((self.request_id, audio_filename, json_filename, callback_url))
        self.application.running_requests[self.request_id] = RequestStates.IN_QUEUE
        self.set_header('X-Request-Id', self.request_id)
        logging.info("Accpted transcription request %s" % self.request_id)


class ResultHandler(tornado.web.RequestHandler):

    def get(self, *args, **kwargs):
        self.set_header("Content-Type", "application/json")
        request_id = self.get_argument("id")
        response = construct_response_json(request_id)
        if request_id in self.application.running_requests:
            response["done"] = False
            state = self.application.running_requests[request_id]
            if state == RequestStates.IN_QUEUE:
                response["message"] = "In queue"
            elif state == RequestStates.PROCESSING:
                response["message"] = "In progress"
        else:
            json_filename = tornado.options.options.user_file_dir + "/" + request_id + ".json"
            if os.path.exists(json_filename):
                response["done"] = True
                result = json.load(open(json_filename))
                response["result"] = result
            else:
                response["done"] = True
                response["error"] = {"code": 1, "message": "Result file for this ID not found"}
        
        self.write(json.dumps(response, sort_keys=False, indent=4))


def main():
    tornado.log.enable_pretty_logging()

    logging.debug('Starting up server')

    tornado.options.define("port", default=8888, help="run on the given port", type=int)
    tornado.options.define("user_file_dir", default="./upload", help="Directory for storing audio and transcriptions files")
    tornado.options.define("transcribe_command", default="speech2text.sh {audio} {json}", help="Command for transcribing an audio file", type=str)
    tornado.options.define("num_transcription_processes", default=10, help="Limit the number of concurrent transcription processes to this value", type=int)
    tornado.options.define("config", type=str, help="path to config file", callback=lambda path: tornado.options.parse_config_file(path, final=True))
    tornado.options.parse_command_line()
    tornado.options.options.user_file_dir = os.path.expanduser(tornado.options.options.user_file_dir)
    app = Application()
    logging.info('Listening on port %d' % tornado.options.options.port)
    app.listen(tornado.options.options.port, max_buffer_size=4*1024*1024*1024) # max upload size is 4GB
    workers = tornado.gen.multi([worker(app) for _ in range(tornado.options.options.num_transcription_processes)])
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
